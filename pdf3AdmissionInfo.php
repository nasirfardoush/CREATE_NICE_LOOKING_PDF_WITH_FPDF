<?php
// include autoloader
include_once('../../vendor/autoload.php');
include_once('../../vendor/dompdf/autoload.inc.php');
// reference the Dompdf namespace
use Dompdf\Dompdf;
$data = "Some thing write";
// reference the Admission  namespace


//Binding HTML Data for pdf view
$html ="
<!DOCTYPE html>
<html>
    <head>
        <title>form</title>
        <style>
            h1,h2,h3{margin: 0; padding: 0}
            .container {
                width: 700px;
                margin: 0 auto;
                font-size:13px;
                 
            }
            .tbl_wraper{height:725px;}
            img {max-width: 100%;}
            .formno{ padding:5px; margin:5px; text-align:center;border-top: 1px solid #0071a2;}
            .title {background-color: #dcddde; padding: 5px 10px; margin-top: 20px}
            .personalIfno {border: 1px solid #dcddde; margin-top: 10px; position:relative;}
            .stimage{position:absolute; width:130px; height:100px; top:3px; right:3px; border:1px solid #ddd; padding-top:50px; text-align:center; }
            tr td:first-child{
               width:150px;
            }           
    
            table tr td:nth-child(2) {
                 width:275px;
            }           

            .notice {padding: 0px 10px; margin-top:15px; margin-bottom:20px;}
            .notice p {padding: 0px; margin:2px; }
            .notice p span{color: red; }
            .signArea {margin-top:50px; text-align:center; height:25px; }
            .signLeft {float: left;width:195px;border-top:1px solid #ddd;margin-left:25px; margin-right:250px;}
            .signRight {float: right;width:195px;border-top:1px solid #ddd;}
             .footer_img {display:block;}
        </style>
    </head>
    <body>
        <div class='container'>
            <img src='../../assets/img/pdf/banner.png' alt=''>
            <div class='formno'>
                <h3>Admission 2017</h3>
                <h3>Form No: </h3>
            </div>
        <div class='tbl_wraper'>    
            <h3 class='title'>Personal Information :</h3>
            <div class='personalIfno'>
                <div class='tableinfo'>
                    <table>
                        <tr>
                            <td>Name</td>
                            <td>: </td>
                        </tr>
                        <tr>
                            <td>Father's Name</td>
                            <td>:  </td>
                        </tr>
                        <tr>
                            <td>Mother's Name</td>
                            <td>: </td>
                        </tr>
                        <tr>
                            <td>Gander</td>
                            <td>: </td>
                        </tr>
                        <tr>
                            <td>Guardian's Name</td>
                            <td>: </td>
                        </tr>
                        <tr>
                            <td>Guardian's Relation</td>
                            <td>: </td>
                        </tr>
                        <tr>
                            <td>Father's Phone Number</td>
                            <td>: </td>
                        </tr>
                        <tr>
                            <td>Guardian's Phone Number</td>
                            <td>: </td>
                        </tr>
                    </table>
                </div>

                <div class='stimage'>2 Copy <br>Photo</div>

            </div>
            <h3 class='title'>Permanent Address :</h3>
            <div class='personalIfno'>
                <div class='tableinfo'>
                    <table>
                        <tr>
                            <td>Village / Area</td>
                            <td>: </td>
                            <td>Police Station</td>
                            <td>: </td>
                        </tr>
                        <tr>
                            <td>Post office</td>
                            <td>: </td>
                            <td>District Name</td>
                            <td>: </td>
                        </tr>
                    </table>
                </div>

            </div>
            <h3 class='title'>Present Address :</h3>
            <div class='personalIfno'>
                <div class='tableinfo'>
                    <table>
                        <tr>
                            <td>House/Road</td>
                            <td>: </td>
                            <td>Sector</td>
                            <td>: </td>
                        </tr>
                        <tr>
                            <td>Area</td>
                            <td>: </td>
                            <td>Post Code</td>
                            <td>: </td>
                        </tr>
                        
                    </table>
                </div>
            </div>
            <h3 class='title'>Academic Information :</h3>
            <div class='personalIfno'>
                <div class='tableinfo'>
                    <table class='mrgless'>
                        <tr>
                            <td>Previous School Name</td>
                            <td>: </td>
                            <td>Which class Done</td>
                            <td>: </td>
                        </tr>
                        <tr>
                            <td>Address Prev. School</td>
                            <td>: </td>
                            <td>Which class Wants</td>
                            <td>: </td>
                        </tr>
                        <tr>
                            <td>T.C Number</td>
                            <td>:  </td>
                        </tr>
                    </table>
                </div>
            </div>
            <div class='notice'>
                <p><span>*</span> Print this form completing online application.</p>
                <p><span>*</span> Attend in time at Iqra Bangladesh School bring with this application </p>
                <p><span>*</span> Attached 2 copy passport size photo (Recently captured). </p>
                <p><span>*</span> Guardian NID Photo copy </p>
            </div>
      </div>
            <div class='signArea'>
                <div class='signLeft'>
                    Student Signature & Date
                </div>                
                <div class='signLeft'>
                    Student Signature & Date
                </div>
            </div>
            <div class='footer_img'>
                <img src='../../assets/img/pdf/footer.jpg' alt='FooterImg'>
            </div>

        </div>
    </body>
</html>

";
 
$dompdf->loadHtml($html);

// (Optional) Setup the paper size and orientation
$dompdf->setPaper('A4', 'portrait');

// Render the HTML as PDF
$dompdf->render();

// Output the generated PDF to Browser
//$dompdf->stream();

// Output the generated PDF (1 = download and 0 = preview)
$dompdf->stream("Admission",array("Attachment"=>0));
?>