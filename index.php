<?php
// include autoloader
require_once 'dompdf/autoload.inc.php';

// reference the Dompdf namespace
use Dompdf\Dompdf;
$data = "Some thing write";
// instantiate and use the dompdf class
$dompdf = new Dompdf();

$html = "
<!DOCTYPE html>
<html>

<head>
    <style>
    body {
        margin: 8px 15px;
        border: 1px solid #0071A2;
    }
   .heading{
		margin-top: 0px;
        width: 100%;
        margin: 0 auto;
        border: none;
        border-collapse: collapse;
   } 
    .heading td {
        padding: 0px;
        text-align: center;
        border-bottom: none;
    }    
    .heading td.picture {
	   width:20%;
	    border: 1px solid #eee;
    }    
    .heading td.logo {
      width:78%;
    }
    p {
        padding: 2px;
        margin: 0px;
    }
    
    header {
        text-align: center;
        background-color: #0071A2;
        padding: 10px;
        width: 100%;
    }
    
    header img {
        max-width: 400;
        max-height: 50px;
    }
    
    section {
        background-color: #eee;
        padding: 5px;
        font-size: 20px;
        border: 2px solid #0071A2;
        text-align: center;
        margin-bottom: 2px;
        width:100%;
    }
    aside{
	   width:100%;
    }
    
    table {
        margin-top: 30px;
        width: 98%;
        margin: 0 auto;
        border: 1px solid #eee;
        border-collapse: collapse;
    }
    
    .title {
        background-color: #eee;
    }
    
    td {
        padding: 5px;
        text-align: left;
        width: 200px;
        border-bottom: 1px solid #ddd;
    }
    
    table span{
   		margin-right:10px;
    }
    footer{
    	padding:5px;
    	width:100%;
    }
    </style>
</head>

<body>
    <table class=heading>
        <tr>
            <td class=logo>
                <header>
       				 <img src=logo.png>
   				 </header>
   				  <section>
			        <strong>Admission Information Form</strong>
			        <p><strong>Form no:20135</strong></p>
			    </section>
            </td>
            <td class=picture>
              <aside>
    			 Picture
    		  </aside>
            </td>
        </tr>
        
    </table>
    
   
    <table>
        <tr>
            <td colspan=2 class=title>
                Personal details
            </td>
        </tr>
        <tr>
            <td>
                <strong>First Name:</strong><span>Abu Ryhan</span>
            </td>
            <td>
                <strong>Last Nmae:</strong><span>Abu</span>
            </td>
        </tr>
        <tr>
            <td>
                <strong>Father's Name:</strong><span>Abu Ryhan</span>
            </td>
            <td>
                <strong>Mother's Name:</strong><span>Abu </span>
            </td>
        </tr>
        <tr>
            <td>
                <strong>Fathers's Phone Number:</strong><span>Abu Ryhan</span>
            </td>
            <td>
                <strong>Guardians's Phone Number:</strong><span>Abu Ryhan</span>
            </td>
        </tr>
        <tr>
            <td>
                <strong>Date of Birth:</strong><span>Abu</span>
            </td>
            <td>
                <strong>Gender:</strong><span>Abu Ryhan</span>
                <strong>Sibling:</strong><span>Abu </span>
            </td>
        </tr>
 		<tr>
            <td>
                <strong>Guardian's Name:</strong><span>Abu Ryhan</span>
            </td>
            <td>
                <strong>Guardian's Relation:</strong><span>Abu Ryhan</span>
            </td>
        </tr> 




         <tr>
            <td colspan=2 class=title>
                Permanent address
            </td>
        </tr>
        <tr>
            <td>
                <strong>Village:</strong><span>Abu Ryhan</span>
            </td>
            <td>
                <strong>Post Office:</strong><span>Abu</span>
            </td>
        </tr>
        <tr>
            <td>
                <strong>Police Station:</strong><span>Abu Ryhan</span>
            </td>
            <td>
                <strong>District:</strong><span>Abu </span>
            </td>
        </tr>







         <tr>
            <td colspan=2 class=title>
                Present Address
            </td>
        </tr>
         <tr>
            <td colspan=2>
                <strong>Area:</strong><span>Abu Ryhan</span>
            </td>
        </tr>       
         <tr>
            <td>
                <strong>Sector:</strong><span>Abu Ryhan</span>
            </td>
            <td>
                <strong>Road Number:</strong><span>Abu </span> 
                <strong>Zip Code:</strong><span>Abu</span>
            </td>
        </tr>
        <tr>
            <td colspan=2>
                <strong>Others:</strong><span>Abu Ryhan</span>
            </td>
        </tr>







         <tr>
            <td colspan=2 class=title>
                 Academic Information
            </td>
        </tr>
        <tr>
            <td>
                <strong>Which class Done ?</strong><span>Abu Ryhan</span>
            </td>
            <td>
                <strong>Which class Wants ?</strong><span>Abu</span>
                <strong>T.C Number:</strong><span>Abu</span>
            </td>
        </tr>
        <tr>
            <td>
                <strong>Privious Shool Name:</strong><span>Abu Ryhan</span>
            </td>
            <td>
                <strong>Privious Shool Address:</strong><span>Abu </span>
            </td>
        </tr>
    </table>
    <hr>
<footer>
	Iqra Bangladesh School <br>
	1158/2, Khilgaon, Chwodhurypara, Dhaka-1219 (+88 01777 447 724, +88 01680 076 613)
	
</footer>

</body>
</html>

";
 
$dompdf->loadHtml($html);

// (Optional) Setup the paper size and orientation
$dompdf->setPaper('A4', 'landscape');

// Render the HTML as PDF
$dompdf->render();

// Output the generated PDF to Browser
//$dompdf->stream();

// Output the generated PDF (1 = download and 0 = preview)
$dompdf->stream("codex",array("Attachment"=>0));
?>