<?php
// include autoloader
include_once('../../vendor/autoload.php');
include_once('../../vendor/dompdf/autoload.inc.php');
// reference the Dompdf namespace
use Dompdf\Dompdf;
$data = "Some thing write";
// reference the Admission  namespace


//Binding HTML Data for pdf view
$html ="
<!DOCTYPE html>
<html>

<head>
    <style>
    body {
        margin: 8px 15px;
        border: 1px solid #0071A2;
        position:relative;
        font-size:13px;
    }
   .heading{
		margin-top: 0px;
        width: 100%;
        margin: 0 auto;
        border: none;
        border-collapse: collapse;
   } 
    .heading td {
        padding: 0px;
        text-align: center;
        border-bottom: none;
    }        
    .heading img {
        width:670px;
        height:60px;
    }    
   td.picture {
        width:100%;
        position:relative;
        height:160px;
    }    
        
    .picture aside{
        position:absolute;
        padding-top:30px;
        right:3px;
        top:3px;
        z-index:1;
        width:125px;
        height:130px;
        border:1px solid #eee;
        text-align:center;
    }

    p {
        padding: 2px;
        margin: 0px;
    }
    
    header {
        text-align: center;
        background-color: #0071A2;
        width: 100%;
    }
    
    section {
        background-color: #eee;
        padding: 5px;
        font-size: 15px;
        border: 2px solid #0071A2;
        text-align: center;
        margin-bottom: 2px;
        width:100%;
    }

    table {
        margin-top: 30px;
        width: 98%;
        margin: 0 auto;
        border: 1px solid #eee;
        border-collapse: collapse;
    }
    
    .title {
        background-color: #eee;
    }
    td {
        padding: 5px;
        text-align: left;
        width: 200px;
        border-bottom: 1px solid #ddd;
    }
    
    table span{
   		margin-right:10px;
        padding:0px 5px;
    }
    .signature{
    	padding-top:70px;
    }
    .step{
        border-bottom:1px solid #eee;
        padding-bottom:15px;
    }
    footer{
    	padding:5px;
    	width:100%;
    	position:absolute;
    	bottom:0px;
    }

    </style>
</head>

<body>
    <table class=heading>
        <tr>
            <td class=logo>
                <header>
       				 <img src=../../assets/img/pdfbaner.jpg>
   				 </header>
   				  <section>
			        <strong>Admission Information Form</strong>
			        <p><strong>Form no:</strong></p>
			    </section>
            </td>
         </tr>     
        
    </table>
    
   
    <table>
        <tr>
            <td colspan=2 class=title>
                Personal details
            </td>
        </tr>
        <tr>
            <td >
                <p><strong>Name:</strong><span>.............................. </span></p>

                 <p><strong>Father's Name:</strong><span>.............................. </span></p>
                   
                 <p><strong>Mother's Name:</strong><span>.............................. </span></p>
                 
                 
                  <p><strong>Gender:</strong><span>............</span>
                      <strong>Sibling:</strong><span>........... </span></p>
               
                 <p><strong>Guardian's Name:</strong><span>..............................</span></p>
              
                 <p><strong>Guardian's Relation:</strong><span>..............................</span></p>
                 
                 <p><strong>Fathers's Phone Number:</strong><span>..............................</span></p>
                 
                 <p><strong>Guardians's Phone Number:</strong><span>..............................</span></p>
                 
            </td>
            <td class=picture>
              <aside>
                 2 copy<br>
                 Photo
              </aside>
            </td>
        </tr>
         <tr>
            <td colspan=2 class=title>
                Permanent address
            </td>
        </tr>
        <tr>
            <td>
                <strong>Village:</strong><span>..............................</span>
            </td>
            <td>
                <strong>Post Office:</strong><span>..............................</span>
            </td>
        </tr>
        <tr>
            <td>
                <strong>Police Station:</strong><span>..............................</span>
            </td>
            <td>
                <strong>District:</strong><span>..............................</span>
            </td>
        </tr>

         <tr>
            <td colspan=2 class=title>
                Present Address
            </td>
        </tr>
         <tr>
            <td colspan=2>
                <strong>Area:</strong><span>..............................</span>
            </td>
        </tr>       
         <tr>
            <td>
                <strong>Sector:</strong><span>.........</span>
            </td>
            <td>
                <strong>Road Number:</strong><span>......... </span> 
                <strong>Zip Code:</strong><span>..........</span>
            </td>
        </tr>
        <tr>
            <td colspan=2>
                <strong>Others:</strong><span>....................................................$others</span>
            </td>
        </tr>



         <tr>
            <td colspan=2 class=title>
                 Academic Information
            </td>
        </tr>
        <tr>
            <td colspan=2>
                <strong>Passing class:</strong><span>.......................</span>
                <strong>Admitted class:</strong><span>.......................</span>
                <strong>T.C Number:</strong><span>.......................</span>
            </td>
        </tr>
        <tr>
            <td>
                <strong>Privious Shool Name:</strong><span>......................................</span>
            </td>
            <td>
                <strong>Privious Shool Address:</strong><span>...................................... </span>
            </td>
        </tr>
    </table>
<article class=signature>
	 <table class=heading>
        <tr>
            <td>
            	.............
            </td>            
            <td>
            ............
            </td>
        </tr>       
         <tr>
            <td>
            	Students
            </td>            
            <td>
            Principale
            </td>
        </tr>
        
    </table>
</article>

<footer>
<article class=step>
<h5>#<u>Admission process</u></h5>
<p>1. Please print this form from completing online application.</p>
<p>2. Attent in time at Iqra Bangladesh School bring with this application.</p>
<p>3. Attchted 2 copy passport size photo (recently captured).</p>

</article>
	Iqra Bangladesh School <br>
	1158/2, Khilgaon, Chwodhurypara, Dhaka-1219 <br>
	+88 01777 447 724, +88 01680 076 613
	
</footer>

</body>
</html>

";
 
$dompdf->loadHtml($html);

// (Optional) Setup the paper size and orientation
$dompdf->setPaper('A4', 'portrait');

// Render the HTML as PDF
$dompdf->render();

// Output the generated PDF to Browser
//$dompdf->stream();

// Output the generated PDF (1 = download and 0 = preview)
$dompdf->stream("Admission",array("Attachment"=>0));
?>